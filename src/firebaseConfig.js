import firebase from 'firebase';
import 'firebase/firestore';

// firebase init goes here
const config = {
  apiKey: 'AIzaSyBPUU5WjzZmn8OkWVYUKl6klVXydBQ7DKY',
  authDomain: 'cofee-time-87a0e.firebaseapp.com',
  databaseURL: 'https://cofee-time-87a0e.firebaseio.com',
  projectId: 'cofee-time-87a0e',
  storageBucket: 'cofee-time-87a0e.appspot.com',
  messagingSenderId: '417143126901',
};
firebase.initializeApp(config);

// firebase utils
const db = firebase.firestore();

// date issue fix according to firebase
const settings = {
  timestampsInSnapshots: true,
};
db.settings(settings);

// firebase collections
const usersCollection = db.collection('users');
const infoCollection = db.collection('info');
export {
  db,
  usersCollection,
  infoCollection,
};
