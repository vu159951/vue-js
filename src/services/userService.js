import { infoCollection, usersCollection } from '../firebaseConfig';

function generateRandom(max, failOn) {
  const coffeed = Array.isArray(failOn) ? failOn : [failOn];
  const randomArray = [];
  for (let i = 1; i <= max; i += 1) {
    if (!coffeed.includes(i)) {
      randomArray.push(i);
    }
    if (coffeed.length === 0) {
      randomArray.push(i);
    }
  }
  const randomIndex = Math.floor(Math.random() * (randomArray.length - 1)) + 1;
  return randomArray[randomIndex - 1];
}

async function createOrUpdateUser(user) {
  await usersCollection.doc(user.name).set(user);
}

async function getUpdatedUser({ coffeeParterId,
  currentCoffeedWith, username, currentUserId, excludeList, totalUser }) {
  const pUser = { name: username, partner: '', isCaffeinated: true, isSubmmited: true, alone: false };
  const partner = await usersCollection.where('id', '==', coffeeParterId)
    .get();
  const parterName = partner.docs[0].data().name;
  const updatedCoffedWith = currentCoffeedWith.concat([coffeeParterId]);
  const coffeTimed = updatedCoffedWith.length;
  await createOrUpdateUser({
    name: username,
    id: currentUserId,
    paired: updatedCoffedWith,
  });
  pUser.partner = parterName;
  pUser.isSubmmited = true;
  pUser.coffeTimed = coffeTimed;
  pUser.isCaffeinated = excludeList.length === (totalUser - 1);
  return pUser;
}

async function pairUser(username) {
  const pUser = { name: username, partner: '', isCaffeinated: true, isSubmmited: true, alone: false };
  const currentUser = await usersCollection.doc(username).get();
  const currentCoffeedWith = currentUser.data().paired;
  const currentUserId = currentUser.data().id;
  const userInfo = await infoCollection.doc('userInfo').get();
  const totalUser = userInfo.data().totalUser;
  const excludeList = currentCoffeedWith.concat(currentUserId);
  const coffeeParterId = generateRandom(totalUser, excludeList);

  if (!coffeeParterId) {
    if (excludeList.length === 1) {
      pUser.alone = true;
      return pUser;
    }
    if (excludeList.length === totalUser) {
      pUser.isCaffeinated = true;
      return pUser;
    }
  }

  const updateUser = await getUpdatedUser({
    coffeeParterId, currentCoffeedWith, username, currentUserId, excludeList, totalUser });
  return updateUser;
}

export { generateRandom, createOrUpdateUser, pairUser };
