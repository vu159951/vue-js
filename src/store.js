import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({ // eslint-disable-line
  state: {
    name: '',
    partner: '',
    coffeTimed: 0,
    isSubmmited: false,
    isCaffeinated: false,
    alone: false,
  },
  actions: {

  },
  mutations: {
    setUser(state, payload) {
      state.name = payload.name;
      state.partner = payload.partner;
      state.coffeTimed = payload.coffeTimed;
      state.isSubmmited = payload.isSubmmited;
      state.isCaffeinated = !!payload.isCaffeinated;
      state.alone = payload.alone;
    },
    setName(state, name) {
      state.name = name;
    },
  },
});
